﻿using System;
using System.Threading.Tasks;
using Android.Bluetooth;
using Android.Util;
using GmVoltDashboard.Obd;
using GmVoltDashboard.Obd.Data;
using GmVoltDashboard.Utility;
using OBD.NET.Common.Devices;
using OBD.NET.Common.Events.EventArgs;
using OBD.NET.Common.OBDData;
using UnitsNet;

namespace GmVoltDashboard.Services
{
	public class ObdService : IDisposable
	{
		private int hvVolts,
					hvAmps;

		private DateTime tsBatterySoc,
						 tsHvVolts,
						 tsHvAmps,
						 tsVehicleSpeed,
						 tsEngineRpm;

		private TaskCompletionSource<object> tcsBatterySoc,
											 tcsHvVolts,
											 tcsHvAmps,
											 tcsVehicleSpeed,
											 tcsEngineRpm;

		private void OnObdCanBusError( object sender, EventArgs eventArgs )
		{
			this.CanError?.Invoke( this, eventArgs );

			this.tcsBatterySoc?.TrySetException( new Exception( "CAN bus error" ) );
			this.tcsEngineRpm?.TrySetException( new Exception( "CAN bus error" ) );
			this.tcsHvAmps?.TrySetException( new Exception( "CAN bus error" ) );
			this.tcsHvVolts?.TrySetException( new Exception( "CAN bus error" ) );
			this.tcsVehicleSpeed?.TrySetException( new Exception( "CAN bus error" ) );

			this.tcsBatterySoc   = null;
			this.tcsEngineRpm    = null;
			this.tcsHvAmps       = null;
			this.tcsHvVolts      = null;
			this.tcsVehicleSpeed = null;
		}

		public event EventHandler CanError;

		public ObdService( BluetoothDevice device )
		{
			this.Connection   =  new BluetoothSerialConnection( device );
			this.ObdHandler   =  new ELM327( this.Connection, new ObdLogger() );
			this.ObdHandler.CanError += this.OnObdCanBusError;
		}

		public  BluetoothSerialConnection Connection      { get; private set; }
		public  double                    BatterySoc      { get; private set; }
		public  double                    InstantPowerKw  => ( this.hvVolts * (double) this.hvAmps ) / 1000.0;
		public  double                    VehicleSpeedMph { get; private set; }
		public  double                    EngineRpm       { get; private set; }
		private ELM327                    ObdHandler      { get; set; }

		public async Task InitializeAsync()
		{
			Log.Debug( MainActivity.LogTag, "Initializing OBD Service" );

			await this.ObdHandler.InitializeAsync();

			this.ObdHandler.SubscribeDataReceived<BatterySoc>( this.OnUpdateBatterySoc );
			this.ObdHandler.SubscribeDataReceived<HvVolts>( this.OnUpdateHvVolts );
			this.ObdHandler.SubscribeDataReceived<HvAmps>( this.OnUpdateHvAmps );
			this.ObdHandler.SubscribeDataReceived<VehicleSpeed>( this.OnUpdateVehicleSpeed );
			this.ObdHandler.SubscribeDataReceived<EngineRPM>( this.OnUpdateEngineRpm );

			Log.Debug( MainActivity.LogTag, "OBD Service initialized" );
		}

		private TaskCompletionSource<object> MakeTaskSource()
		{
			return new TaskCompletionSource<object>().WithTimeout( TimeSpan.FromMilliseconds( 500 ) );
		}

		#region Update requests

		public async Task UpdateAsync()
		{
			await Task.WhenAll(
				this.UpdateBatterySocAsync(),
				this.UpdateHvVoltsAsync(),
				this.UpdateHvAmpsAsync(),
				this.UpdateVehicleSpeedAsync(),
				this.UpdateEngineRpmAsync()
			);
		}

		public Task UpdateBatterySocAsync()
		{
			if ( this.tcsBatterySoc?.Task.IsFinished() == false )
				return this.tcsBatterySoc.Task;

#if DEBUG
			Log.Debug( MainActivity.LogTag, "Requesting battery SOC..." );
#endif

			this.tcsBatterySoc = this.MakeTaskSource();
			this.ObdHandler.RequestData<BatterySoc>();

			return this.tcsBatterySoc.Task;
		}

		public async Task UpdateHvVoltsAsync()
		{
			if ( this.tcsHvVolts?.Task.IsFinished() == false )
			{
				await this.tcsHvVolts.Task;
				return;
			}

#if DEBUG
			Log.Debug( MainActivity.LogTag, "Requesting HV Volts..." );
#endif

			this.tcsHvVolts = this.MakeTaskSource();
			this.ObdHandler.RequestData<HvVolts>();

			await this.tcsHvVolts.Task;
		}

		public async Task UpdateHvAmpsAsync()
		{
			if ( this.tcsHvAmps?.Task.IsFinished() == false )
			{
				await this.tcsHvAmps.Task;
				return;
			}

#if DEBUG
			Log.Debug( MainActivity.LogTag, "Requesting HV Amps..." );
#endif

			this.tcsHvAmps = this.MakeTaskSource();
			this.ObdHandler.RequestData<HvAmps>();

			await this.tcsHvAmps.Task;
		}

		public Task UpdateVehicleSpeedAsync()
		{
			if ( this.tcsVehicleSpeed?.Task.IsFinished() == false )
				return this.tcsVehicleSpeed.Task;

#if DEBUG
			Log.Debug( MainActivity.LogTag, "Requesting vehicle speed..." );
#endif

			this.tcsVehicleSpeed = this.MakeTaskSource();
			this.ObdHandler.RequestData<VehicleSpeed>();

			return this.tcsVehicleSpeed.Task;
		}

		public Task UpdateEngineRpmAsync()
		{
			if ( this.tcsEngineRpm?.Task.IsFinished() == false )
				return this.tcsEngineRpm.Task;

#if DEBUG
			Log.Debug( MainActivity.LogTag, "Requesting engine RPM..." );
#endif

			this.tcsEngineRpm = this.MakeTaskSource();
			this.ObdHandler.RequestData<EngineRPM>();

			return this.tcsEngineRpm.Task;
		}

		#endregion

		#region Update data handlers

		private void OnUpdateBatterySoc( object sender, DataReceivedEventArgs<BatterySoc> args )
		{
			if ( args.Timestamp >= this.tsBatterySoc )
			{
#if DEBUG
				Log.Debug( MainActivity.LogTag, $"Got battery SOC: {args.Data.SocPercent}" );
#endif

				this.BatterySoc = args.Data.SocPercent;
				this.tcsBatterySoc?.TrySetResult( null );
				this.tcsBatterySoc = null;
				this.tsBatterySoc  = args.Timestamp;
			}
		}

		private void OnUpdateHvVolts( object sender, DataReceivedEventArgs<HvVolts> args )
		{
			if ( args.Timestamp >= this.tsHvVolts )
			{
#if DEBUG
				Log.Debug( MainActivity.LogTag, $"Got HV Volts: {args.Data.Volts}" );
#endif

				this.hvVolts = args.Data.Volts;
				this.tcsHvVolts?.TrySetResult( null );
				this.tcsHvVolts = null;
				this.tsHvVolts  = args.Timestamp;
			}
		}

		private void OnUpdateHvAmps( object sender, DataReceivedEventArgs<HvAmps> args )
		{
			if ( args.Timestamp >= this.tsHvAmps )
			{
#if DEBUG
				Log.Debug( MainActivity.LogTag, $"Got HV Amps: {args.Data.Amps}" );
#endif

				this.hvAmps = args.Data.Amps;
				this.tcsHvAmps?.TrySetResult( null );
				this.tcsHvAmps = null;
				this.tsHvAmps  = args.Timestamp;
			}
		}

		private void OnUpdateVehicleSpeed( object sender, DataReceivedEventArgs<VehicleSpeed> args )
		{
			if ( args.Timestamp >= this.tsVehicleSpeed )
			{
#if DEBUG
				Log.Debug( MainActivity.LogTag, $"Got vehicle speed: {args.Data.Speed.Value}" );
#endif

				this.VehicleSpeedMph = Speed.FromKilometersPerHour( args.Data.Speed.Value ).MilesPerHour;
				this.tcsVehicleSpeed?.TrySetResult( null );
				this.tcsVehicleSpeed = null;
				this.tsVehicleSpeed  = args.Timestamp;
			}
		}

		private void OnUpdateEngineRpm( object sender, DataReceivedEventArgs<EngineRPM> args )
		{
			if ( args.Timestamp >= this.tsEngineRpm )
			{
#if DEBUG
				Log.Debug( MainActivity.LogTag, $"Got engine RPM: {args.Data.Rpm.Value}" );
#endif

				this.EngineRpm = args.Data.Rpm.Value;
				this.tcsEngineRpm?.TrySetResult( null );
				this.tcsEngineRpm = null;
				this.tsEngineRpm  = args.Timestamp;
			}
		}

		#endregion

		private bool disposed;

		public void Dispose()
		{
			if ( !this.disposed )
			{
				this.ObdHandler?.Dispose();
				this.ObdHandler = null;
				this.Connection?.Dispose();
				this.Connection = null;

				this.disposed = true;
			}
		}
	}
}