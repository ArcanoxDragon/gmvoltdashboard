﻿using System;
using System.IO;
using System.Threading.Tasks;
using Android.Bluetooth;
using Android.Util;
using Android.Widget;
using GmVoltDashboard.Utility;

namespace GmVoltDashboard.Services
{
	public class VoltService
	{
		private readonly BluetoothDevice device;
		private readonly ToastService    toastService;
		private          ObdService      obdService;
		private          bool            running;
		private          bool            initialized;
		private          Task            runningTask;

		public VoltService( BluetoothDevice device, ToastService toastService )
		{
			this.device       = device;
			this.toastService = toastService;
			this.obdService   = new ObdService( device );
		}

		protected double EvBatterySoc => ( ( this.obdService.BatterySoc * 255 / 100 ) - Constants.Volt.CsRawSocOffset ) / Constants.Volt.CsRawSocMult;

		protected double CsBatterySoc => MathUtil.MapRange( this.obdService.BatterySoc,
															Constants.Volt.BatteryPercentCsLower,
															Constants.Volt.BatteryPercentCsUpper,
															0.0,
															100.0,
															clipOutput: false );

		public double EngineOnThreshold
		{
			get
			{
				if ( this.EngineOn )
					return -1;

				return MathUtil.MapRange( this.obdService.VehicleSpeedMph, 15, 35, 27, 15 );
			}
		}

		public double RegenCoefficient
		{
			get
			{
				var maxRegenForSpeed = MathUtil.MapRange( this.obdService.VehicleSpeedMph,
														  Constants.Volt.RegenCurveMinMph, Constants.Volt.RegenCurveMaxMph,
														  Constants.Volt.RegenCurveMinKw, Constants.Volt.RegenCurveMaxKw );

				return Math.Max( 0, -this.PowerKw ) / maxRegenForSpeed;
			}
		}

		public double RegenEfficiency => this.obdService.VehicleSpeedMph < Constants.Volt.RegenCurveMinMph
											 ? 1.0
											 : MathUtil.MapRange( this.RegenCoefficient,
																  Constants.Volt.RegenCoefMaxEfficiency, Constants.Volt.RegenCoefMinEfficiency,
																  1.0, 0.0 );

		public double BatteryPercent => this.IsCsMode ? this.CsBatterySoc : this.EvBatterySoc;
		public double PowerKw        => this.obdService.InstantPowerKw;
		public bool   EngineOn       => this.obdService.EngineRpm > 100;
		public bool   IsCsMode       { get; private set; }

		public void Start()
		{
			if ( this.running )
				return;

			this.runningTask = Task.Run( this.UpdateLoop );
		}

		public async Task StopAsync()
		{
			if ( !this.running )
				return;

			this.running = false;
			await this.runningTask;
		}

		private async Task UpdateLoop()
		{
			Log.Debug( MainActivity.LogTag, "Initializing Volt service" );

			while ( !this.initialized )
			{
				this.toastService.ShowToast( "Connecting to OBD adapter...", ToastLength.Long );

				if ( await this.ConnectAsync() )
				{
					this.toastService.ShowToast( "Connected!" );
					this.initialized = true;
				}
				else
				{
					// Connection timed out; try again
					this.toastService.ShowToast( "Connection failed. Retrying in 2 seconds..." );
					await Task.Delay( TimeSpan.FromSeconds( 2 ) );
				}
			}

			var ticksSinceFullUpdate = 0;
			var reconnect            = false;

			this.running = true;

			while ( this.running )
			{
				if ( reconnect )
				{
					Log.Error( MainActivity.LogTag, "Bluetooth socket got disconnected. Reconnecting..." );
					this.toastService.ShowToast( "OBD disconnected...reconnecting", ToastLength.Long );

					if ( await this.ConnectAsync() )
					{
						Log.Info( MainActivity.LogTag, "Reconnected." );
						this.toastService.ShowToast( "Reconnected!" );
						reconnect = false;
					}
					else
					{
						Log.Info( MainActivity.LogTag, "Could not reconnect...retrying" );
						this.toastService.ShowToast( "Could not reconnect...retrying in 2 seconds" );
						await Task.Delay( TimeSpan.FromSeconds( 2 ) );
					}
				}
				else
				{
					try
					{
						if ( ticksSinceFullUpdate++ >= 5 )
						{
							ticksSinceFullUpdate = 0;
							await this.obdService.UpdateAsync();
						}
						else
						{
							// Only update things we need more frequently
							await Task.WhenAll(
								this.obdService.UpdateHvAmpsAsync(),
								this.obdService.UpdateVehicleSpeedAsync()
							);
						}
					}
					catch ( TaskCanceledException )
					{
						Log.Debug( MainActivity.LogTag, "An update task was cancelled, likely because an OBD command timed out" );
					}
					catch ( Java.Lang.Exception ex )
					{
						Log.Error( MainActivity.LogTag, ex, "Java Exception while updating ObdService" );
					}
					catch ( Exception ex )
					{
						Log.Error( MainActivity.LogTag, ".NET Exception while updating ObdService:" );
						Log.Error( MainActivity.LogTag, ex.ToString() );
					}

					var batt = this.obdService.BatterySoc;

					if ( batt >= Constants.Volt.BatteryPercentCsUpper + 5 )
						this.IsCsMode = false;
					else if ( batt <= Constants.Volt.BatteryPercentCsThreshold )
						this.IsCsMode = true;

					if ( !this.obdService.Connection.IsOpen || this.obdService.Connection.HasError )
						reconnect = true;

					await Task.Delay( 100 );
				}
			}
		}

		private async Task<bool> ConnectAsync()
		{
			try
			{
				this.obdService?.Dispose();
				this.obdService = new ObdService( this.device );
				await this.obdService.InitializeAsync();

				return true;
			}
			catch ( Exception )
			{
				return false;
			}
		}
	}
}