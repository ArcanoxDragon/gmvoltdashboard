﻿using Android.App;
using Android.Content;
using Android.Widget;

namespace GmVoltDashboard.Services
{
	public class ToastService
	{
		private readonly Activity contextActivity;

		public ToastService( Activity contextActivity )
		{
			this.contextActivity = contextActivity;
		}

		public void ShowToast( string message, ToastLength toastLength = ToastLength.Short )
		{
			this.contextActivity.RunOnUiThread( () => Toast.MakeText( this.contextActivity, message, toastLength ).Show() );
		}
	}
}