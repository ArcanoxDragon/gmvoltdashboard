﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Android.App;
using Android.Bluetooth;
using Android.Content;
using Android.Content.PM;
using Android.OS;
using Android.Preferences;
using Android.Support.Design.Widget;
using Android.Views;
using Android.Widget;
using GmVoltDashboard.Controllers;
using GmVoltDashboard.Settings;
using GmVoltDashboard.Utility;
using GmVoltDashboard.Views;
using Java.Lang;
using Microsoft.AppCenter;
using Microsoft.AppCenter.Analytics;
using Microsoft.AppCenter.Crashes;
using PerpetualEngine.Storage;
using Exception = System.Exception;

namespace GmVoltDashboard
{
	[ Activity(
		Label             = "Volt Dashboard",
		MainLauncher      = true,
		Icon              = "@mipmap/icon",
		Theme             = "@style/Theme.AppCompat.NoActionBar",
		ScreenOrientation = ScreenOrientation.Portrait,
		ConfigurationChanges = ConfigChanges.KeyboardHidden |
							   ConfigChanges.Orientation |
							   ConfigChanges.ScreenLayout |
							   ConfigChanges.ScreenSize |
							   ConfigChanges.UiMode ) ]
	public class MainActivity : Activity, ISharedPreferencesOnSharedPreferenceChangeListener
	{
		public const string LogTag = "GmVoltDashboard";

		private MainController controller;
		private DateTime       lastBackButton;

		public override void OnBackPressed()
		{
			if ( ( DateTime.Now - this.lastBackButton ).TotalSeconds < 2 )
				JavaSystem.Exit( 0 );
			else
				Toast.MakeText( this, "Press \"back\" again to exit", ToastLength.Short ).Show();

			this.lastBackButton = DateTime.Now;
		}

		protected override async void OnCreate( Bundle savedInstanceState )
		{
			base.OnCreate( savedInstanceState );

			this.SetupAnalytics();

			// App settings
			SimpleStorage.SetContext( this.ApplicationContext );

			// Enter fullscreen mode and keep screen on
			this.Window.AddFlags( WindowManagerFlags.Fullscreen | WindowManagerFlags.KeepScreenOn );

			// Set our view from the "main" layout resource
			this.SetContentView( Resource.Layout.Main );

			this.HookNavigation();

			var mainView = this.FindViewById<MainView>( Resource.Id.mainView );

			// First run
			var storage = SimpleStorage.EditGroup( "VoltDashboard" );
			if ( !storage.HasKey( "HasFirstRun" ) )
			{
				storage.Put( "HasFirstRun", true );
				var dialog = new AlertDialog.Builder( this )
							 .SetTitle( "Set Up OBD Adapter" )
							 .SetMessage( "Since you have not set up this app yet, you will need to configure" +
										  "the bluetooth OBD adapter in the app settings. Use the left-side" +
										  "slide-out drawer to access app settings." )
							 .SetCancelable( true )
							 .SetNeutralButton( "Ok", ( s, a ) => { } )
							 .Create();

				dialog.Show();
			}

			if ( this.controller != null )
				await this.controller.StopAsync();

			this.controller = new MainController( this, mainView );

			var preferences = PreferenceManager.GetDefaultSharedPreferences( this );
			preferences.RegisterOnSharedPreferenceChangeListener( this );

			var deviceAddress = preferences.GetString( SettingsFragment.ObdDeviceKey, null );

			if ( deviceAddress != null )
				await this.InitializeWithDeviceAsync( deviceAddress );
			else
				Toast.MakeText( this, "Please configure an OBD Bluetooth device in settings", ToastLength.Long ).Show();
		}

		private async Task InitializeWithDeviceAsync( string deviceAddress )
		{
			var adapter = BluetoothAdapter.DefaultAdapter;

			if ( adapter == null )
			{
				var dialog = new AlertDialog.Builder( this )
							 .SetTitle( "Bluetooth Not Found" )
							 .SetMessage( "A Bluetooth adapter was not found on this device." +
										  "This app requires a Bluetooth adapter." )
							 .SetPositiveButton( "Exit", ( s, a ) => { } )
							 .Create();
				dialog.Show();
				JavaSystem.Exit( 0 );
				return;
			}

			if ( !adapter.IsEnabled )
			{
				var turnOn = false;
				var dialog = new AlertDialog.Builder( this )
							 .SetTitle( "Bluetooth Disabled" )
							 .SetMessage( "Bluetooth is turned off. You must turn it on to use" +
										  "this app. Turn it on now?" )
							 .SetPositiveButton( "Yes", ( s, a ) => turnOn = true )
							 .SetNegativeButton( "No", ( s, a ) => { } )
							 .Create();
				dialog.Show();

				if ( turnOn )
				{
					adapter.Enable();
				}
				else
				{
					JavaSystem.Exit( 0 );
					return;
				}
			}

			var device = adapter.BondedDevices.FirstOrDefault( d => d.Address == deviceAddress );

			if ( device == null )
			{
				Toast.MakeText( this, "Selected OBD device no longer paired. Choose a new one in Settings.", ToastLength.Long ).Show();
				var preferences = PreferenceManager.GetDefaultSharedPreferences( this );
				preferences.Edit().PutString( SettingsFragment.ObdDeviceKey, null ).Commit();
			}
			else
			{
				await this.controller.StartAsync( device );
			}
		}

		private void HookNavigation()
		{
			var navView = this.FindViewById<NavigationView>( Resource.Id.navigation );
			navView.NavigationItemSelected += this.NavigationItemSelected;
		}

		private void NavigationItemSelected( object sender, NavigationView.NavigationItemSelectedEventArgs args )
		{
			switch ( args.MenuItem.ItemId )
			{
				case Resource.Id.nav_settings:
					args.Handled = true;
					this.StartActivity( typeof( SettingsActivity ) );
					break;
			}
		}

		private void SetupAnalytics()
		{
			AppCenter.Start( PrivateConstants.AnalyticsKey,
							 typeof( Analytics ),
							 typeof( Crashes ) );

			AppDomain.CurrentDomain.UnhandledException += ( sender, args ) => Crashes.TrackError( args.ExceptionObject as System.Exception );
			TaskScheduler.UnobservedTaskException += ( sender, args ) => Crashes.TrackError( args.Exception );
		}

		public async void OnSharedPreferenceChanged( ISharedPreferences sharedPreferences, string key )
		{
			if ( key == SettingsFragment.ObdDeviceKey )
			{
				var deviceAddress = sharedPreferences.GetString( key, null );
				await this.InitializeWithDeviceAsync( deviceAddress );
			}
		}
	}
}