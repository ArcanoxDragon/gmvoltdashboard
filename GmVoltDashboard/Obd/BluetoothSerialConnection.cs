﻿using System;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Android.Bluetooth;
using Android.Util;
using GmVoltDashboard.Utility;
using OBD.NET.Common.Communication;
using OBD.NET.Common.Communication.EventArgs;

namespace GmVoltDashboard.Obd
{
	public class BluetoothSerialConnection : ISerialConnection
	{
		private readonly BluetoothSocket         socket;
		private readonly CancellationTokenSource disposeToken;
		private          bool                    reading;

		public BluetoothSerialConnection( BluetoothDevice device )
		{
			this.socket       = device.CreateInsecureRfcommSocketToServiceRecord( Constants.Bluetooth.BtSerialUuid );
			this.disposeToken = new CancellationTokenSource();
		}

		public bool                                      HasError { get; private set; }
		public bool                                      IsOpen   => this.socket.IsConnected;
		public bool                                      IsAsync  => true;
		public event EventHandler<DataReceivedEventArgs> DataReceived;

		public void Connect()
		{
			this.socket.Connect();
			this.PostConnect();
		}

		public async Task ConnectAsync()
		{
			var timeoutSource = new CancellationTokenSource( TimeSpan.FromSeconds( Constants.Bluetooth.ConnectionTimeout ) );
			var connectTask   = this.socket.ConnectAsync();

			while ( !connectTask.IsCompleted )
			{
				await Task.Delay( 100 );

				if ( timeoutSource.IsCancellationRequested )
				{
					this.Dispose();
					throw new IOException( "Bluetooth connection timed out" );
				}
			}

			await connectTask;

			this.PostConnect();
		}

		private void PostConnect()
		{
			Task.Run( this.BeginReadAsync, this.disposeToken.Token ).ConfigureAwait( false );
		}

		private async Task BeginReadAsync()
		{
			this.reading = true;
			var buffer = new byte[ 1024 ];
			var stream = this.socket.InputStream;

			Log.Debug( MainActivity.LogTag, "Connected to device and ready to read data" );

			while ( this.reading )
			{
				try
				{
					if ( stream.CanRead )
					{
						var bytesRead = await stream.ReadAsync( buffer, 0, buffer.Length, this.disposeToken.Token );
						this.DataReceived?.Invoke( this, new DataReceivedEventArgs( bytesRead, buffer.Take( bytesRead ).ToArray() ) );
					}
					else
					{
						await Task.Delay( 10 );
					}
				}
				catch ( Java.Lang.Exception ex )
				{
					Log.Error( MainActivity.LogTag, ex.Cause, "An error occurred while reading from a Bluetooth socket" );
					this.reading  = false;
					this.HasError = true;
				}
				catch ( Exception ex )
				{
					Log.Error( MainActivity.LogTag, "An error occurred while reading from a Bluetooth socket" );
					Log.Error( MainActivity.LogTag, ex.ToString() );
					this.reading  = false;
					this.HasError = true;
				}
			}
		}

		public void Write( byte[] data ) => this.socket.OutputStream.Write( data, 0, data.Length );
		public Task WriteAsync( byte[] data ) => this.socket.OutputStream.WriteAsync( data, 0, data.Length );

		#region Dispose

		private bool disposed;

		public void Dispose()
		{
			if ( !this.disposed )
			{
				this.disposed = true;

				try
				{
					this.disposeToken.Cancel( false );
				}
				catch
				{
					// Ignored
				}

				this.socket.Close();
				this.socket.Dispose();

				this.reading = false;
			}
		}

		#endregion
	}
}