﻿using GmVoltDashboard.Utility;
using OBD.NET.Common.Attributes;
using OBD.NET.Common.OBDData;

namespace GmVoltDashboard.Obd.Data
{
	[ ObdMode( Constants.Obd.Modes.VoltReadHybridData ) ]
	public class HvAmps : AbstractOBDData
	{
		public HvAmps() : base( 0x2414, 2 ) { }

		public int Amps => ( (sbyte) this.A * 256 + this.B ) / 20;
	}
}