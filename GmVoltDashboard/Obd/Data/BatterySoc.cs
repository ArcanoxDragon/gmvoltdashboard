﻿using OBD.NET.Common.OBDData;

namespace GmVoltDashboard.Obd.Data
{
	public class BatterySoc : AbstractOBDData
	{
		public BatterySoc() : base( 0x5B, 1 ) { }

		public double SocPercent => this.A * ( 100.0 / 255.0 );
	}
}