﻿using GmVoltDashboard.Utility;
using OBD.NET.Common.Attributes;
using OBD.NET.Common.OBDData;

namespace GmVoltDashboard.Obd.Data
{
	[ ObdMode( Constants.Obd.Modes.VoltReadHybridData ) ]
	public class HvVolts : AbstractOBDData
	{
		public HvVolts() : base( 0x2429, 2 ) { }

		public int Volts => ( (sbyte) this.A * 256 + this.B ) / 64;
	}
}