﻿using Android.Util;
using OBD.NET.Common.Logging;

namespace GmVoltDashboard.Obd
{
	public class ObdLogger : IOBDLogger
	{
		public void WriteLine( string text, OBDLogLevel level )
		{
			switch ( level )
			{
				case OBDLogLevel.Debug:
					Log.Debug( MainActivity.LogTag, text );
					break;
				case OBDLogLevel.Error:
					Log.Error( MainActivity.LogTag, text );
					break;
				case OBDLogLevel.Verbose:
					Log.Verbose( MainActivity.LogTag, text );
					break;
				default:
					Log.Info( MainActivity.LogTag, text );
					break;
			}
		}
	}
}