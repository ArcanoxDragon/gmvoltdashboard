﻿using System.Threading.Tasks;

namespace GmVoltDashboard.Utility
{
    public static class TaskExtensions
    {
		public static bool IsFinished( this Task task )
		{
			return task.IsCompleted || task.IsCanceled || task.IsFaulted;
		}
    }
}
