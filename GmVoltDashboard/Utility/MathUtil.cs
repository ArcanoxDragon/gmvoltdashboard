﻿namespace GmVoltDashboard.Utility
{
	public static class MathUtil
	{
		public static double MapRange( double value, double fromMin, double fromMax, double toMin, double toMax, bool clipOutput = true )
		{
			if ( clipOutput && value <= fromMin )
				return toMin;
			if ( clipOutput && value >= fromMax )
				return toMax;

			var fromWidth = fromMax - fromMin;
			var toWidth = toMax - toMin;
			var rawValue = ( value - fromMin ) / fromWidth;

			return rawValue * toWidth + toMin;
		}
	}
}