﻿using Android.Graphics;
using Java.Util;

namespace GmVoltDashboard.Utility
{
	public class Constants
	{
		public class Colors
		{
			public static readonly Color BgColorNormal = new Color( 74,  177, 217 );
			public static readonly Color BgColorEv     = new Color( 175, 227, 17 );

			public static readonly Color BrakeEfficiencyColorNormal = new Color( 123, 255, 59 );
			public static readonly Color BrakeEfficiencyColorBad    = new Color( 255, 72,  0 );
		}

		public class Bluetooth
		{
			public static readonly UUID BtSerialUuid = UUID.FromString( "00001101-0000-1000-8000-00805F9B34FB" );

			public const double ConnectionTimeout = 12.0;
		}

		public class Obd
		{
			public class Modes
			{
				public const byte VoltReadHybridData = 0x22;
			}
		}

		public class Volt
		{
			public const int BatteryPercentCsUpper     = 20;
			public const int BatteryPercentCsLower     = 10;
			public const int BatteryPercentCsThreshold = 18;

			public const double CsRawSocOffset = 40;
			public const double CsRawSocMult   = 1.90735;

			public const double RegenCurveMaxKw        = 50;
			public const double RegenCurveMaxMph       = 50;
			public const double RegenCurveMinKw        = 5;
			public const double RegenCurveMinMph       = 5;
			public const double RegenCoefMinEfficiency = 0.98;
			public const double RegenCoefMaxEfficiency = 0.9;
		}
	}
}