﻿using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;
using Android.Animation;

namespace GmVoltDashboard.Utility
{
	public static class AnimatorExtensions
	{
		private static readonly ConditionalWeakTable<Animator, CancellationTokenSource> AnimatorCancellationSources = new ConditionalWeakTable<Animator, CancellationTokenSource>();

		public static void StartAsync( this Animator animator )
		{
			animator.CancelAsync();
			var cancellationSource = new CancellationTokenSource();
			Task.Run( animator.Start, cancellationSource.Token );
			AnimatorCancellationSources.Add( animator, cancellationSource );
		}

		public static void CancelAsync( this Animator animator )
		{
			if ( AnimatorCancellationSources.TryGetValue( animator, out var cts ) )
			{
				try
				{
					cts.Cancel( false );
				}
				catch
				{
					//ignore
				}

				AnimatorCancellationSources.Remove( animator );
			}

			animator.Cancel();
		}

		public static void TryCancel( this Animator animator )
		{
			if ( animator == null )
				return;

			try
			{
				animator.Cancel();
			}
			catch
			{
				//ignored
			}
		}
	}
}