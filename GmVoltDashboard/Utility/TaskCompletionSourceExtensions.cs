﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace GmVoltDashboard.Utility
{
    public static class TaskCompletionSourceExtensions
    {
		public static TaskCompletionSource<T> WithTimeout<T>( this TaskCompletionSource<T> taskCompletionSource, TimeSpan timeout )
		{
			var cancellationTokenSource = new CancellationTokenSource( timeout );

			cancellationTokenSource.Token.Register( () => {
				taskCompletionSource.TrySetCanceled();
			} );

			return taskCompletionSource;
		}
    }
}
