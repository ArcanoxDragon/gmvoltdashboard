﻿using System;
using Android.Animation;
using Android.Content;
using Android.Graphics;
using Android.Runtime;
using Android.Util;
using Android.Views.Animations;
using Android.Widget;
using GmVoltDashboard.Utility;

namespace GmVoltDashboard.Views
{
	public class MainView : LinearLayout
	{
		#region Constants

		// Measurement

		private const double GaugeTopPct       = 166.0 / 2560.0; // Computed from pixel measurements in image
		private const double GaugeBottomPct    = 2193.0 / 2560.0;
		private const double GaugeMiddlePct    = 1172.0 / 2560.0;
		private const double EvThresholdRefPct = 1192.0 / 2560.0;

		// Gauge values

		private const int AccelMaxKw   = 120;
		private const int AccelMaxKwEv = 35;
		private const int BrakeMaxKw   = 65;

		// Animation
		private const int AnimationDurationMs = 500;

		#endregion

		// Paint objects
		private Paint bgPaint,
					  evPaint,
					  evThresholdPaint,
					  brakePaint;

		// Evaluators
		private ArgbEvaluator brakeColorEvaluator;

		// Bitmaps
		private Bitmap bmpBackground,
					   bmpOverlay,
					   bmpEV,
					   bmpBatteryBar,
					   bmpAccelBar,
					   bmpBrakeBar,
					   bmpEVThresholdCS;

		// Layout
		private bool layoutInitialized;


		private Rect rectBatteryBarSrc,
					 rectBatteryBarDst;

		private Rect rectAccelBrakeBarSrc,
					 rectAccelBrakeBarDst;

		private Rect rectEVThresholdDst;

		public MainView( Context context ) : base( context ) => this.Init();
		public MainView( Context context, IAttributeSet attrs ) : base( context, attrs ) => this.Init();
		public MainView( Context context, IAttributeSet attrs, int defStyleAttr ) : base( context, attrs, defStyleAttr ) => this.Init();
		public MainView( Context context, IAttributeSet attrs, int defStyleAttr, int defStyleRes ) : base( context, attrs, defStyleAttr, defStyleRes ) => this.Init();
		protected MainView( IntPtr javaReference, JniHandleOwnership transfer ) : base( javaReference, transfer ) => this.Init();

		#region UI updates

		private ObjectAnimator backgroundColorAnimator,
							   evLabelAnimator,
							   evThresholdFadeAnimator;

		protected void ChangeBackgroundColor( Color newColor )
		{
			this.backgroundColorAnimator.TryCancel();
			this.backgroundColorAnimator = ObjectAnimator.OfObject( this.bgPaint, "Color", new ArgbEvaluator(),
																	this.bgPaint.Color.ToArgb(),
																	newColor.ToArgb() );
			this.backgroundColorAnimator.SetDuration( AnimationDurationMs );
			this.backgroundColorAnimator.Update += ( s, a ) => this.PostInvalidate();
			this.Post( this.backgroundColorAnimator.Start );
		}

		protected void SetEvLabelVisible( bool visible )
		{
			this.evLabelAnimator.TryCancel();
			this.evLabelAnimator = ObjectAnimator.OfObject( this.evPaint, "Color", new ArgbEvaluator(),
															this.evPaint.Color.ToArgb(),
															( visible ? Color.White : Color.Transparent ).ToArgb() );
			this.evLabelAnimator.SetDuration( AnimationDurationMs );
			this.evLabelAnimator.Update += ( s, a ) => this.PostInvalidate();
			this.Post( this.evLabelAnimator.Start );
		}

		protected void SetEvThresholdVisible( bool visible )
		{
			this.evThresholdFadeAnimator.TryCancel();
			this.evThresholdFadeAnimator = ObjectAnimator.OfObject( this.evThresholdPaint, "Color", new ArgbEvaluator(),
																	this.evThresholdPaint.Color.ToArgb(),
																	( visible ? Color.White : Color.Transparent ).ToArgb() );
			this.evThresholdFadeAnimator.SetDuration( AnimationDurationMs );
			this.evThresholdFadeAnimator.Update += ( s, a ) => this.PostInvalidate();
			this.Post( this.evThresholdFadeAnimator.Start );
		}

		#endregion

		#region Initialization

		private void Init()
		{
			this.SetWillNotDraw( false ); // Layout has its own drawing

			this.InitPaint();
			this.LoadResources();
			this.CalculateLayout();
		}

		private void InitPaint()
		{
			this.bgPaint = new Paint { Color = Constants.Colors.BgColorEv };
			this.bgPaint.SetStyle( Paint.Style.Fill );

			this.evPaint = new Paint { Color = Color.White };
			this.evPaint.SetStyle( Paint.Style.Fill );

			this.evThresholdPaint = new Paint { Color = Color.White };
			this.evThresholdPaint.SetStyle( Paint.Style.Fill );

			this.brakePaint = new Paint();
			this.brakePaint.SetColorFilter( new PorterDuffColorFilter( Constants.Colors.BrakeEfficiencyColorNormal, PorterDuff.Mode.SrcIn ) );
			this.brakeColorEvaluator = new ArgbEvaluator();
		}

		private void LoadResources()
		{
			this.bmpBackground    = BitmapFactory.DecodeResource( this.Resources, Resource.Drawable.Background );
			this.bmpOverlay       = BitmapFactory.DecodeResource( this.Resources, Resource.Drawable.Overlay );
			this.bmpEV            = BitmapFactory.DecodeResource( this.Resources, Resource.Drawable.EV_Label );
			this.bmpBatteryBar    = BitmapFactory.DecodeResource( this.Resources, Resource.Drawable.Battery_Bar );
			this.bmpAccelBar      = BitmapFactory.DecodeResource( this.Resources, Resource.Drawable.Accel_Bar );
			this.bmpBrakeBar      = BitmapFactory.DecodeResource( this.Resources, Resource.Drawable.Brake_Bar );
			this.bmpEVThresholdCS = BitmapFactory.DecodeResource( this.Resources, Resource.Drawable.CS_EV_Threshold );
		}

		#endregion

		#region Model

		// Battery gauge percent

		private float         batteryBarPercent;
		private ValueAnimator batteryBarAnimator;

		public float BatteryBarPercent
		{
			get => this.batteryBarPercent;
			set
			{
				if ( Math.Abs( value - this.batteryBarPercent ) < float.Epsilon )
					return;

				this.batteryBarAnimator.TryCancel();
				this.batteryBarAnimator = ValueAnimator.OfFloat( this.batteryBarPercent, value );
				this.batteryBarAnimator.SetDuration( AnimationDurationMs );
				this.batteryBarAnimator.SetInterpolator( new DecelerateInterpolator() );
				this.batteryBarAnimator.Update += ( s, a ) => {
					this.batteryBarPercent = (float) a.Animation.AnimatedValue;
					this.CalculateLayout();
					this.PostInvalidate();
				};
				this.Post( this.batteryBarAnimator.Start );
			}
		}

		// EV mode

		private bool isEVMode;

		public bool IsEVMode
		{
			get => this.isEVMode;
			set
			{
				if ( this.isEVMode == value )
					return;

				this.isEVMode = value;
				this.ChangeBackgroundColor( value
												? Constants.Colors.BgColorEv
												: Constants.Colors.BgColorNormal );
				this.SetEvLabelVisible( value );
			}
		}

		// EV threshold

		private float         evThreshold;
		private ValueAnimator evThresholdAnimator;

		public float EvThreshold
		{
			get => this.evThreshold;
			set
			{
				if ( Math.Abs( value - this.evThreshold ) < float.Epsilon )
					return;

				this.SetEvThresholdVisible( value > 0 );

				this.evThresholdAnimator.TryCancel();
				this.evThresholdAnimator = ValueAnimator.OfFloat( this.evThreshold, value );
				this.evThresholdAnimator.SetDuration( AnimationDurationMs );
				this.evThresholdAnimator.SetInterpolator( new DecelerateInterpolator() );
				this.evThresholdAnimator.Update += ( s, a ) => {
					this.evThreshold = (float) a.Animation.AnimatedValue;
					this.CalculateLayout();
					this.PostInvalidate();
				};
				this.Post( this.evThresholdAnimator.Start );
			}
		}

		// Accel/brake power

		private float         accelBrakePower;
		private ValueAnimator accelBrakeAnimator;

		public float AccelBrakePower
		{
			get => this.accelBrakePower;
			set
			{
				if ( Math.Abs( value - this.accelBrakePower ) < float.Epsilon )
					return;

				this.accelBrakeAnimator.TryCancel();
				this.accelBrakeAnimator = ValueAnimator.OfFloat( this.accelBrakePower, value );
				this.accelBrakeAnimator.SetDuration( AnimationDurationMs );
				this.accelBrakeAnimator.SetInterpolator( new DecelerateInterpolator() );
				this.accelBrakeAnimator.Update += ( s, a ) => {
					this.accelBrakePower = (float) a.Animation.AnimatedValue;
					this.CalculateLayout();
					this.PostInvalidate();
				};
				this.Post( this.accelBrakeAnimator.Start );
			}
		}

		// Accel kW range

		private int           accelMaxKw = AccelMaxKw;
		private ValueAnimator maxKwAnimator;

		public bool FullKwRange
		{
			get => this.accelMaxKw == AccelMaxKw;
			set
			{
				if ( value == this.FullKwRange )
					return;

				this.maxKwAnimator.TryCancel();
				this.maxKwAnimator = ValueAnimator.OfInt( this.accelMaxKw, value ? AccelMaxKw : AccelMaxKwEv );
				this.maxKwAnimator.SetDuration( AnimationDurationMs );
				this.maxKwAnimator.SetInterpolator( new DecelerateInterpolator() );
				this.maxKwAnimator.Update += ( s, a ) => {
					this.accelMaxKw = (int) a.Animation.AnimatedValue;
					this.CalculateLayout();
					this.PostInvalidate();
				};
				this.Post( this.maxKwAnimator.Start );
			}
		}

		// Brake kW Color
		private float         brakeEfficiency;
		private ValueAnimator brakeEfficiencyAnimator;

		public float BrakeEfficiency
		{
			get => this.brakeEfficiency;
			set
			{
				if ( Math.Abs( value - this.BrakeEfficiency ) < float.Epsilon )
					return;

				this.brakeEfficiencyAnimator.TryCancel();
				this.brakeEfficiencyAnimator = ValueAnimator.OfFloat( this.brakeEfficiency, value );
				this.brakeEfficiencyAnimator.SetDuration( AnimationDurationMs );
				this.brakeEfficiencyAnimator.SetInterpolator( new DecelerateInterpolator() );
				this.brakeEfficiencyAnimator.Update += ( s, a ) => {
					this.brakeEfficiency = (float) a.Animation.AnimatedValue;

					// ReSharper disable ImpureMethodCallOnReadonlyValueField
					var targetBrakeColor = (int) this.brakeColorEvaluator.Evaluate(
						this.brakeEfficiency,
						Constants.Colors.BrakeEfficiencyColorBad.ToArgb(),
						Constants.Colors.BrakeEfficiencyColorNormal.ToArgb()
					);
					// ReSharper restore ImpureMethodCallOnReadonlyValueField

					this.brakePaint.SetColorFilter( new PorterDuffColorFilter( new Color( targetBrakeColor ), PorterDuff.Mode.SrcIn ) );
					this.CalculateLayout();
					this.PostInvalidate();
				};
				this.Post( this.brakeEfficiencyAnimator.Start );
			}
		}

		#endregion

		private void CalculateLayout()
		{
			if ( !this.layoutInitialized )
			{
				this.rectBatteryBarSrc = new Rect( 0, 0, this.bmpBatteryBar.Width, this.bmpBatteryBar.Height );
				this.rectBatteryBarDst = new Rect( 0, 0, this.Width,               this.Height );

				this.rectAccelBrakeBarSrc = new Rect( 0, 0, this.bmpAccelBar.Width, this.bmpAccelBar.Height );
				this.rectAccelBrakeBarDst = new Rect( 0, 0, this.Width,             this.Height );

				this.rectEVThresholdDst = new Rect( 0, 0, this.Width, this.Height );

				this.layoutInitialized = true;
			}

			// Battery gauge
			var batBarUiPercent = MathUtil.MapRange( this.BatteryBarPercent, 0.0, 100.0, GaugeBottomPct, GaugeTopPct );
			this.rectBatteryBarSrc.Top = (int) ( batBarUiPercent * this.bmpBatteryBar.Height );
			this.rectBatteryBarDst.Top = (int) ( batBarUiPercent * this.Height );

			// Accel/brake gauge
			if ( this.AccelBrakePower >= 0.0 )
			{
				var accelBrakeBarUiPercent = MathUtil.MapRange( this.AccelBrakePower, 0.0, this.accelMaxKw, GaugeMiddlePct, GaugeTopPct );
				this.rectAccelBrakeBarSrc.Top    = (int) ( accelBrakeBarUiPercent * this.bmpAccelBar.Height );
				this.rectAccelBrakeBarDst.Top    = (int) ( accelBrakeBarUiPercent * this.Height );
				this.rectAccelBrakeBarSrc.Bottom = this.bmpAccelBar.Height;
				this.rectAccelBrakeBarDst.Bottom = this.Height;
			}
			else
			{
				var accelBrakeBarUiPercent = MathUtil.MapRange( -this.AccelBrakePower, 0.0, BrakeMaxKw, GaugeMiddlePct, GaugeBottomPct );
				this.rectAccelBrakeBarSrc.Top    = 0;
				this.rectAccelBrakeBarDst.Top    = 0;
				this.rectAccelBrakeBarSrc.Bottom = (int) ( accelBrakeBarUiPercent * this.bmpBrakeBar.Height );
				this.rectAccelBrakeBarDst.Bottom = (int) ( accelBrakeBarUiPercent * this.Height );
			}

			// EV threshold
			if ( this.EvThreshold >= 0.0 )
			{
				var evThresholdUiPercent = MathUtil.MapRange( this.EvThreshold, 0, this.accelMaxKw, 0, GaugeTopPct - EvThresholdRefPct );
				this.rectEVThresholdDst.Top    = (int) ( evThresholdUiPercent * this.Height );
				this.rectEVThresholdDst.Bottom = this.rectEVThresholdDst.Top + this.Height;
			}
		}

		protected override void OnSizeChanged( int w, int h, int oldw, int oldh )
		{
			this.layoutInitialized = false;
			this.CalculateLayout();
		}

		protected override void OnDraw( Canvas canvas )
		{
			// Draw background
			canvas.DrawRect( 0, 0, canvas.Width, canvas.Height, this.bgPaint );
			canvas.DrawBitmap( this.bmpBackground, null, canvas.ClipBounds, null );

			// Draw gauges
			canvas.DrawBitmap( this.bmpBatteryBar, this.rectBatteryBarSrc, this.rectBatteryBarDst, null );
			canvas.DrawBitmap( this.AccelBrakePower >= 0 ? this.bmpAccelBar : this.bmpBrakeBar,
							   this.rectAccelBrakeBarSrc,
							   this.rectAccelBrakeBarDst,
							   this.AccelBrakePower >= 0 ? null : this.brakePaint );

			if ( this.EvThreshold >= 0 )
				canvas.DrawBitmap( this.bmpEVThresholdCS, null, this.rectEVThresholdDst, this.evThresholdPaint );

			// Draw overlays
			canvas.DrawBitmap( this.bmpOverlay, null, canvas.ClipBounds, null );
			canvas.DrawBitmap( this.bmpEV,      null, canvas.ClipBounds, this.evPaint );
		}
	}
}