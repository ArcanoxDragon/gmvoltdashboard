﻿using System.Linq;
using Android.App;
using Android.Bluetooth;
using Android.OS;
using Android.Preferences;

namespace GmVoltDashboard.Settings
{
	public class SettingsFragment : PreferenceFragment
	{
		public const string ObdSettingsHeaderKey = "pref_key_obd_settings";
		public const string ObdDeviceKey = "pref_key_obd_device";

		public override void OnCreate( Bundle savedInstanceState )
		{
			base.OnCreate( savedInstanceState );

			this.AddPreferencesFromResource( Resource.Xml.settings );
			this.PopulateDevices();
		}

		private void PopulateDevices()
		{
			if ( !( this.FindPreference( ObdDeviceKey ) is ListPreference preference ) )
				return;

			var adapter = BluetoothAdapter.DefaultAdapter;

			if ( adapter == null )
			{
				var dialog = new AlertDialog.Builder( this.Context )
					.SetTitle( "Bluetooth Required" )
					.SetMessage( "Bluetooth is required to use an OBD adapter." +
								 "A Bluetooth adapter was not found on this device." )
					.SetPositiveButton( "Ok", ( s, a ) => { } )
					.Create();

				dialog.Show();
				this.FragmentManager.PopBackStack();
				return;
			}

			if ( !adapter.IsEnabled )
			{
				var turnOn = false;
				var dialog = new AlertDialog.Builder( this.Context )
					.SetTitle( "Bluetooth turned off" )
					.SetMessage( "Bluetooth is turned off. Would you like to turn it on?" )
					.SetNegativeButton( "No", ( s, a ) => { } )
					.SetPositiveButton( "Yes", ( s, a ) => turnOn = true )
					.Create();

				dialog.Show();

				if ( !turnOn )
				{
					this.FragmentManager.PopBackStack();
					return;
				}

				adapter.Enable();
			}

			var devices = adapter.BondedDevices;
			var deviceNames = devices.Select( d => d.Name ).ToArray();
			var deviceValues = devices.Select( d => d.Address ).ToArray();

			preference.SetEntries( deviceNames );
			preference.SetEntryValues( deviceValues );
			preference.Summary = "%s";
		}
	}
}