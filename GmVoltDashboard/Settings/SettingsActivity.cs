﻿using Android.App;
using Android.Content.PM;
using Android.OS;

namespace GmVoltDashboard.Settings
{
	[ Activity(
		Label = "Volt Dashboard Settings",
		Icon = "@mipmap/icon",
		ScreenOrientation = ScreenOrientation.Portrait,
		ParentActivity = typeof( MainActivity ) ) ]
	public class SettingsActivity : Activity
	{
		protected override void OnCreate( Bundle savedInstanceState )
		{
			base.OnCreate( savedInstanceState );

			this.ActionBar.SetDisplayHomeAsUpEnabled( true );
			this.FragmentManager.BeginTransaction()
				.Replace( Android.Resource.Id.Content, new SettingsFragment() )
				.Commit();
		}
	}
}