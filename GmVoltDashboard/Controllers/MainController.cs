﻿using System;
using System.Threading.Tasks;
using Android.Bluetooth;
using GmVoltDashboard.Services;
using GmVoltDashboard.Views;

namespace GmVoltDashboard.Controllers
{
	public class MainController
	{
		private readonly MainActivity    activity;
		private readonly MainView        view;
		private          bool            running;
		private          BluetoothDevice device;
		private          VoltService     voltService;
		private          Task            runningTask;

		public MainController( MainActivity activity, MainView view )
		{
			this.activity = activity;
			this.view     = view;
		}

		public async Task StartAsync( BluetoothDevice device )
		{
			await this.StopAsync();

			this.device = device;

			this.runningTask = Task.Run( this.UpdateLoop );
		}

		public async Task StopAsync()
		{
			if ( !this.running )
				return;

			this.running = false;
			await this.runningTask;

			if ( this.voltService != null )
				await this.voltService.StopAsync();
		}

		private async Task UpdateLoop()
		{
			await Task.Delay( 1000 );

			if ( this.device == null )
				return;

			if ( this.voltService != null )
				await this.voltService.StopAsync();

			this.voltService = new VoltService( this.device, new ToastService( this.activity ) );
			this.voltService.Start();

			var lastPower           = -1.0;
			var lastBattery         = -1.0;
			var lastEvThreshold     = -1.0;
			var lastEvMode          = false;
			var lastKwRangeMode     = true; // full
			var lastBrakeEfficiency = 1.0;

			this.running = true;
			while ( this.running )
			{
				try
				{
					var power           = this.voltService.PowerKw;
					var battery         = this.voltService.BatteryPercent;
					var evThreshold     = this.voltService.EngineOnThreshold;
					var evMode          = !this.voltService.EngineOn;
					var fullKwRange     = this.voltService.EngineOn || !this.voltService.IsCsMode;
					var brakeEfficiency = this.voltService.RegenEfficiency;

					if ( Math.Abs( power - lastPower ) > double.Epsilon )
						this.view.AccelBrakePower = (float) power;
					if ( Math.Abs( battery - lastBattery ) > double.Epsilon )
						this.view.BatteryBarPercent = (float) battery;
					if ( Math.Abs( evThreshold - lastEvThreshold ) > double.Epsilon )
						this.view.EvThreshold = (float) evThreshold;
					if ( evMode != lastEvMode )
						this.view.IsEVMode = evMode;
					if ( fullKwRange != lastKwRangeMode )
						this.view.FullKwRange = fullKwRange;
					if ( Math.Abs( brakeEfficiency - lastBrakeEfficiency ) > double.Epsilon )
						this.view.BrakeEfficiency = (float) brakeEfficiency;

					lastPower           = power;
					lastBattery         = battery;
					lastEvThreshold     = evThreshold;
					lastEvMode          = evMode;
					lastKwRangeMode     = fullKwRange;
					lastBrakeEfficiency = brakeEfficiency;

					await Task.Delay( 10 );
				}
				catch ( Exception e )
				{
					Console.WriteLine( e );
					throw;
				}
			}
		}
	}
}